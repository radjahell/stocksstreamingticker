﻿using System;
using Interfaces;
using Orleans.Runtime;
using Orleans.Core;
using Microsoft.Extensions.Logging;
using Orleans;
using System.Threading.Tasks;
using Intrinio.SDK.Client;
using Intrinio.SDK.Api;
using System.Diagnostics;
using Intrinio.SDK.Model;

namespace Grains.Services
{
    public class StocksProxyGrainService : GrainService, IStocksProxyGrainService
    {
        public IGrainFactory GrainFactory { get; }
        RealtimeStockPrice result = null;

        public StocksProxyGrainService(IServiceProvider services,
                                  IGrainIdentity id,
                                  Silo silo,
                                  ILoggerFactory loggerFactory,
                                  IGrainFactory grainFactory)
                 : base(id, silo, loggerFactory)
        {

            GrainFactory = grainFactory;
        }

        public async Task<string> GetPriceQuoteAsync(string ticker)
        {
            Configuration.Default.AddApiKey("api_key", "OmJmYmJmYThiNGY5NmY0ZTVhNjI1ZDQzMmU5ZGJkODcx");

            var securityApi = new SecurityApi();

            try
            {
                result = await securityApi.GetSecurityRealtimePriceAsync(ticker, null);
                Debug.WriteLine(result);
            }
            catch (Exception e)
            {
                Debug.Print("Exception when calling SecurityApi.GetSecurityRealtimePrice: " + e.Message);
            }

            return result.LastPrice.ToString();
        }
    }

} 