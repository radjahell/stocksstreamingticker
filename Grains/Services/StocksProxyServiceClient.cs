﻿using System;
using System.Threading.Tasks;
using Interfaces;
using Orleans.Runtime.Services;

namespace Grains.Services
{
    public class StocksProxyServiceClient : GrainServiceClient<IStocksProxyGrainService>, IStocksProxyServiceClient
    {
        public StocksProxyServiceClient(IServiceProvider serviceProvider) 
            : base(serviceProvider)
        {
        }

        public Task<string> GetPriceQuoteAsync(string ticker) => GrainService.GetPriceQuoteAsync(ticker);
    }
}
