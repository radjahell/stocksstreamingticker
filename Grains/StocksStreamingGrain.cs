﻿using System;
using System.Threading.Tasks;
using Interfaces;
using Orleans;
using Orleans.Runtime;

namespace Grains
{
    public class StocksStreamingGrain : Grain, IStocksStreamingGrain
    {
        private string price;
        private readonly IStocksProxyServiceClient _stocksProxyServiceClient;

        public StocksStreamingGrain(IGrainActivationContext grainActivationContext,
                                    IStocksProxyServiceClient stocksProxyServiceClient)
        {
            _stocksProxyServiceClient = stocksProxyServiceClient;
        }

        public override async Task OnActivateAsync()
        {
            var ticker = this.GetPrimaryKeyString();

            await UpdatePrice(ticker);

            RegisterTimer(UpdatePrice,
                          ticker,
                          TimeSpan.FromSeconds(10),
                          TimeSpan.FromSeconds(10)); 

            await base.OnActivateAsync();
        }


        private async Task UpdatePrice(object stock)
        { 
            price = await GetPriceQuote(stock as string);
            Console.WriteLine(price); 
        } 

        private async Task<string> GetPriceQuote(string ticker)
        {
            return await _stocksProxyServiceClient.GetPriceQuoteAsync(ticker);
        }

        public Task<string> GetPrice()
        {
            return Task.FromResult(price);
        }
    }
}
