﻿using System.Threading.Tasks;
using Orleans.Services;

namespace Interfaces
{
    public interface IStocksProxyGrainService : IGrainService
    {
          Task<string> GetPriceQuoteAsync(string ticker);
    }
}
