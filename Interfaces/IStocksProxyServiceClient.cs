﻿
using Orleans.Services;

namespace Interfaces
{
    public interface IStocksProxyServiceClient : IGrainServiceClient<IStocksProxyGrainService>, IStocksProxyGrainService
    {   
    }
}
