﻿
using System;
using System.Threading.Tasks;
using Grains;
using Grains.Services;
using Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Orleans;
using Orleans.Configuration;
using Orleans.Hosting;

namespace SiloHost
{
    class Program
    {
        static int Main(string[] args)
        {
            return RunSilo().Result;
        } 

        private static async Task<int> RunSilo()
        { 
            try
            {
                var host = await StartSilo(); 

                Console.WriteLine("Press Enter to terminate silo...");
                Console.ReadLine();

                await host.StopAsync();

                return 0;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return -1;
            } 
        }


        private static async Task<ISiloHost> StartSilo()
        {
            var builder = new SiloHostBuilder()
                .UseLocalhostClustering()
                .AddGrainService<StocksProxyGrainService>()
                .ConfigureServices(services =>
                {
                    services.AddSingleton<IStocksProxyServiceClient, StocksProxyServiceClient>();
                })
                .Configure<ClusterOptions>(options =>
                {
                    options.ClusterId = "dev";
                    options.ServiceId = "StocksTickerApp";
                })
                .UseDashboard()
                .Configure<EndpointOptions>(options => options.AdvertisedIPAddress = System.Net.IPAddress.Loopback)
                .ConfigureApplicationParts(parts => parts.AddApplicationPart(typeof(StocksStreamingGrain).Assembly).WithReferences());

            var silo = builder.Build();
            await silo.StartAsync();

            return silo; 
        }

    }
}
